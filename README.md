1 after cloning the repository, make sure the docker daemon is running on your machine
2 buid the docker image: docker build --rm -t "go" --no-cache .
3 launch the container: docker run -dti -p 8080:8080 --name "test" go
4 open the IP of the host on your browser with 8080 port: 100.100.100.100:8080
5 click on the button "test technest web server" and wait for 30 seconds (no progress signals available)
6 enjoy the fantastic golang web application!