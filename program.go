package main

import (
        "html/template"
	"log"
	"net/http"
	"os/exec"
)

var tmpl = template.Must(template.New("tmpl").ParseFiles("index.html"))

func execCommandHandler(w http.ResponseWriter, r *http.Request) {
	
	output, err := exec.Command("echo", "<br /><br />##### LOOKING FOR DNS PROVIDER #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output)

	output1, err := exec.Command("dig", "www.technest.es", "NS").Output()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(output1)

	output2, err := exec.Command("echo", "<br /><br />##### TESTING SERVER PERFORMANCE #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output2)

        output3, err := exec.Command("ab", "-k", "-n10", "-c10", "-t5", "https://www.technest.es/").Output()
        if err != nil {
                http.Error(w, err.Error(), 418)
                return
        }
        w.Write(output3)

        output4, err := exec.Command("echo", "<br /><br />##### LOOKING FOR OPEN PORTS #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output4)

	output5, err := exec.Command("nmap", "www.technest.es").Output()
        if err != nil {
                http.Error(w, err.Error(), 418)
                return
        }
        w.Write(output5)

	output6, err := exec.Command("echo", "<br /><br />##### TRACING ROUTE TO HOST #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output6)

        output7, err := exec.Command("traceroute", "www.technest.es").Output()
        if err != nil {
                http.Error(w, err.Error(), 418)
                return
        }
        w.Write(output7)

	output8, err := exec.Command("echo", "<br /><br />##### CURRENT WEB SERVER STATUS #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output8)

        output9, err := exec.Command("curl", "-I", "http://www.technest.es").Output()
        if err != nil {
                http.Error(w, err.Error(), 418)
                return
        }
        w.Write(output9)

        output10, err := exec.Command("ping", "-c1", "www.technest.es").Output()
        if err != nil {
                http.Error(w, "<br /><br />##### NO PING RESPONSE #####<br /><br />", 418)
        }
        w.Write(output10)

        output11, err := exec.Command("echo", "<br /><br />##### MALAGA WEATHER #####<br /><br />").Output()
        if err != nil {
                http.Error(w, err.Error(), http.StatusInternalServerError)
                return
        }
        w.Write(output11)

        output12, err := exec.Command("curl", "http://api.openweathermap.org/data/2.5/weather?q=Malaga,es&APPID=ba2a6339ad87a77ba5948efedc4f1b4a").Output()
        if err != nil {
                http.Error(w, err.Error(), 418)
                return
        }
        w.Write(output12)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	if err := tmpl.ExecuteTemplate(w, "index.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/exec", execCommandHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
