FROM centos:latest

MAINTAINER Jose Luis

LABEL version 0.1

RUN yum update -y && yum install -y \
epel-release \
bind-utils \
nmap \
httpd-tools \
traceroute

RUN yum install -y golang

COPY program.go /
COPY index.html /

EXPOSE 8080

WORKDIR /

CMD ["go", "run", "program.go"]
